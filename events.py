from watchdog.events import RegexMatchingEventHandler

VIDEOFILE_PATTERN = r"(.*)\.(mkv|mov)$"

class VideoMatchingEventHandler(RegexMatchingEventHandler):

    def on_created(self, event):
        print 'A videofile was added', event

