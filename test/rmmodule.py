
import os
import os.path

class RemovalService(object):

    def rm(filename):
        if os.path.isfile(filename):
            os.remove(filename)
