from rmmodule import RemovalService

import unittest

import mock

@mock.patch('rmmodule.os.path')
@mock.patch('rmmodule.os')
class RmTestCase(unittest.TestCase):

    def test_rm(self, mock_os, mock_path):

        reference = RemovalService()

        mock_path.isfile.return_value = False

        reference.rm('any path')

        self.assertFalse(mock_os.remove.called, "Failed to not remove the file if not present.")

        mock_path.isfile.return_value = True

        reference.rm('any path')

        mock_os.remove.assert_called_with('any path')

if __name__ == '__main__':
    unittest.main()