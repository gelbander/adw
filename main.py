import sys
import time
import logging
from watchdog.observers import Observer
from watchdog.events import LoggingEventHandler, PatternMatchingEventHandler, FileSystemEventHandler, \
    RegexMatchingEventHandler

from events import VideoMatchingEventHandler

VIDEOFILE_PATTERN = r"(.*)\.(mkv|mov)$"

class VideoFile(object):

    def __init__(self, file_name, path, size, file_format):
        self.file_name = file_name
        self.path = path
        self.size = size
        self.file_format = file_format

if __name__ == '__main__':

    # Creates a stream handler with a basic formatter.
    logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s - %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S')

    path = sys.argv[1] if len(sys.argv) > 1 else '.'

    # VideoMatchingEventHandler is a subclass of FileSystemEventHandler
    event_handler = VideoMatchingEventHandler([VIDEOFILE_PATTERN])

    # Platform-independent observer that polls a directory to detect file
    # system changes
    observer = Observer()

    # Inject the eventhandler and a path for the observer to watch, recursively.
    observer.schedule(event_handler, path, recursive=True)

    # Start watching
    observer.start()

    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        # Exit on key interruption.
        observer.stop()

    # Join the threads.
    observer.join()
